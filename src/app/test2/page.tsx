"use client"

import { useState } from 'react';
import { addDays, format } from 'date-fns';
import { DateRange  , DayPicker } from 'react-day-picker'
import 'react-day-picker/dist/style.css';

import "./style.css"
const pastMonth = new Date();

const page = () => {
    let Para = <p> chose time </p>
    const defaultSelected: DateRange = {
            from: pastMonth,
            to: addDays(pastMonth, 4)
    };
    const [range, setRange] 
    = useState<DateRange | undefined>(defaultSelected)
    if(range?.from){
        if(!range?.to){
            Para=<p>{format(range?.from, 'PPP')}  </p> 
        }else if(range?.to){
            Para=<p>{format(range?.from, 'PPP')}  - {format(range?.to, 'PPP')} </p> 
        }
    }
    
    return (
        <div>
            {Para}
            
        <DayPicker 
        id="example"
        numberOfMonths={2}
        //  defaultMonth
        pagedNavigation
        fixedWeeks
        mode="range"
        // required
        selected={range}
        onSelect={setRange}
        showOutsideDays
        className="styling"
        />

        </div>
    )
}

export default page
