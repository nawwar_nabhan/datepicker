"use client"
import Image from 'next/image'
import { useState } from 'react'
// import { DateRangePicker } from 'rsuite'
import styles from './page.module.css'

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css"

export default function Home() {
    const [dateRange, setDateRange] = useState([null, null]);
    const [startDate, endDate] = dateRange;
    return (
    <DatePicker
      selectsRange={true}
      startDate={startDate}
      endDate={endDate}
      onChange={(update : any) => {
        setDateRange(update);
      }}
      isClearable={true}
    />
  )
}

