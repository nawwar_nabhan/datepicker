import React from "react";

export default function LL (
    props : {
        children  : React.ReactNode,
        paral : React.ReactNode,
        second : React.ReactNode
    }
) {
    console.log(props.paral);
    return (
        <div>
            {props.children}
            {props.paral}
            {props.second}
        </div>
    )
}