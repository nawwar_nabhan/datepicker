'use client'
// import { useRouter } from "next/router"
import { useRouter } from 'next/navigation'
import { useState } from "react"

export default function RT(){
    const router = useRouter()
    const [state, setState] = useState(false)
    return (
        <div>
            <button 
                onClick= {
                    ()=>{
                        router.push("/about/12312")
                        // setState(!state)
                    }
                }
            >
                    go to your page
            </button>

            {
                state ? 
                    <div> 
                        state is true
                    </div>
                    :
                    <div>
                        state is false
                    </div>
            }
        </div>
    )
}