"use client"
import { DateRangePicker } from "rsuite";

export  default function () {
       return (
        <div>      <p>Date Time Range</p>
        <DateRangePicker
      format="yyyy-MM-dd HH:mm:ss"
      defaultCalendarValue={[new Date(), new Date()]}
    />
        </div>
    )
}