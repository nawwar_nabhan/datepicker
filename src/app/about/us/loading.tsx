import Image from "next/image";

export default  function LoadingUs () {
    return (
         <div>
             <Image
                src="https://picsum.photos/id/237/200/300"
                alt = 'test loading'
                width="300"
                height="200"
             ></Image>
         </div>
    )
}