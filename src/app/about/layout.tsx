import Footer from "./Footer";
import Header from "./Header";

export default function AboutLayout (
    {children} : {
        children : React.ReactNode
    }
) {
    return (
        <article>
            <Header/>
                {children}
            <Footer/>
        </article>
    )
}