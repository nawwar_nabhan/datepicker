import React from "react";

export default function Layout (
    {children} : {
        children : React.ReactNode
    }
) {
    return(
        <section>
                <header>
                    data from header
                </header>
                {children}
                <footer>
                    data from footer
                </footer>
        </section>
    )
}