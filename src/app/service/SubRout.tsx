'use client'

import { useRouter } from "next/navigation"
    
export default function SubRoute () {    
    const router = useRouter()

    const handleClick = ()=>{
        router.push("service/subservice")
    }
    return(
        <div>
            <button 
                onClick={handleClick}
            >
                move to sub service 
            </button>

        </div>
    )
}