/** @type {import('next').NextConfig} */
const nextConfig = {
    images : {
        domains  : ["picsum.photos"]
    },
    distDir: '_static',
    output: 'export',
}

module.exports = nextConfig
